package ca.unb.lostandfound.view;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.Result;

import ca.unb.lostandfound.R;
import ca.unb.lostandfound.service.CoordinateUtils;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QrReaderFragment extends Fragment implements ZXingScannerView.ResultHandler {

    private final String TAG = "QrReaderFragment";

    // Range is 0 (whole world) to 21 (individual buildings)
    private final int MAPS_ZOOM_LEVEL = 21;

    private ZXingScannerView scannerView;
    private TextureView textureView;
    private BottomNavigationView bottomNavigationView;
    private View rootView;
    private enum QrCodeDataResult {NO_COORDINATES, INVALID_COORDINATES, VALID_COORDINATES};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_qr_reader, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        scannerView = view.findViewById(R.id.scanner_view);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void handleResult(Result result) {
        Log.d(TAG, "QR code scanner results: " + result.getText());
        String description;
        QrCodeDataResult dataResult = determineQrCodeDataResults(result.getText());

        switch (dataResult) {
            case NO_COORDINATES:
                description = getString(R.string.snackbar_no_coordinates_description);
                displayNegativeQrReadingResults(description);
                break;
            case INVALID_COORDINATES:
                description = getString(R.string.snackbar_invalid_coordinates_description);
                displayNegativeQrReadingResults(description);
                break;
            case VALID_COORDINATES:
                double[] coordinates = CoordinateUtils.parseCoordinates(result.getText());
                String latitude = String.valueOf(coordinates[0]);
                String longitude = String.valueOf(coordinates[1]);
                description = getString(R.string.snackbar_valid_coordinates_title) +
                        "\n" +
                        getString(R.string.snackbar_valid_coordinates_description, latitude, longitude);
                displayPositiveQrReadingResults(description, coordinates);
                break;
        }
    }

    private QrCodeDataResult determineQrCodeDataResults(String results) {
        double[] coordinates = CoordinateUtils.parseCoordinates(results);
        QrCodeDataResult dataResult;

        if (coordinates.length != 2) {
            dataResult = QrCodeDataResult.NO_COORDINATES;
        }
        else if (!CoordinateUtils.validateCoordinates(coordinates)) {
            dataResult = QrCodeDataResult.INVALID_COORDINATES;
        }
        else {
            dataResult = QrCodeDataResult.VALID_COORDINATES;
        }

        return dataResult;
    }

    private void displayNegativeQrReadingResults(String description) {
        View rootView = getView().getRootView().findViewById(R.id.qr_reader_container);
        final Snackbar snackbar = Snackbar.make(rootView, description, Snackbar.LENGTH_LONG);
        snackbar.setTextColor(Color.WHITE);

        String dismissAction = getString(R.string.snackbar_action_dismiss);
        snackbar.setAction(dismissAction, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scannerView.resumeCameraPreview(QrReaderFragment.this);
                snackbar.dismiss();
            }
        });

        // Pause the scanner until the snackbar disappears
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                scannerView.resumeCameraPreview(QrReaderFragment.this);
            }

            @Override
            public void onShown(Snackbar snackbar) {

            }
        });

        // Ensures that the snackbar appears above the navigation bar
        snackbar.setAnchorView(bottomNavigationView);
        snackbar.show();
    }

    private void displayPositiveQrReadingResults(String description, final double[] coordinates) {
        View rootView = getView().getRootView().findViewById(R.id.qr_reader_container);
        final Snackbar snackbar = Snackbar.make(rootView, description, Snackbar.LENGTH_LONG);
        snackbar.setTextColor(Color.WHITE);

        String mapsAction = getString(R.string.snackbar_action_maps);
        snackbar.setAction(mapsAction, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = createGoogleMapsIntent(coordinates);
                startActivity(mapIntent);
            }
        });

        // Pause the scanner until the snackbar disappears
        snackbar.addCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                scannerView.resumeCameraPreview(QrReaderFragment.this);
            }

            @Override
            public void onShown(Snackbar snackbar) {

            }
        });

        // Ensures that the snackbar appears above the navigation bar
        snackbar.setAnchorView(bottomNavigationView);
        snackbar.show();
    }

    private Intent createGoogleMapsIntent(double[] coordinates) {
        String locationUri = "geo:" + coordinates[0] + "," + coordinates[1] + "?z=" + MAPS_ZOOM_LEVEL;
        Uri intentUri = Uri.parse(locationUri);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, intentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        return mapIntent;
    }
}
