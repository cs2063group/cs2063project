package ca.unb.lostandfound.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.print.PrintHelper;

import com.squareup.picasso.Picasso;

import java.io.File;

import ca.unb.lostandfound.R;
import ca.unb.lostandfound.service.OnImageClickListener;

public class FullSizedGalleryImageActivity extends AppCompatActivity {

    private final String TAG = "FullSizedGalleryImageActivity";

    private String url;
    private String imageName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_sized_gallery_image);
        final ImageView imageView = findViewById(R.id.full_sized_image);

        url = getIntent().getStringExtra("image_url");
        Log.d(TAG, "Retrieving [" + url + "] to be displayed in fullscreen mode");

        // Enables the back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        imageName = new File(url).getName();
        getSupportActionBar().setTitle(imageName);

        loadImage(url, imageView);

        imageView.setOnClickListener(new OnImageClickListener() {

            @Override
            public void onClick(View v) {
                boolean isSelected = !imageView.isSelected();
                imageView.setSelected(isSelected);

                if (isSelected) {
                    hideUiDecor(getWindow());
                }
                else {
                    showUiDecor(getWindow());
                }
            }
        });
    }

    private void loadImage(String url, ImageView imageView){
        File file = new File(url);
        Picasso.get()
                .load(file)
                .into(imageView);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.actionbar_image_share:
                shareImageToApps();
                break;
            case R.id.actionbar_image_open_with:
                openWith();
                break;
            case R.id.actionbar_image_get_info:
                getAdditionalDetails();
                break;
            case R.id.actionbar_image_print:
                printImage();
                break;
            case R.id.actionbar_image_delete:
                deleteImageDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_image_main_menu, menu);
        setMenuBarIconColour(menu);

        return true;
    }

    private void setMenuBarIconColour(Menu menu) {
        for(int i = 0; i < menu.size(); i++){
            Drawable drawable = menu.getItem(i).getIcon();
            if(drawable != null) {
                drawable.mutate();
                drawable.setTint(getColor(R.color.colorOnPrimary));
            }
        }
    }

    private void shareImageToApps() {
        File imageToShare = new File(url);

        String authority = getString(R.string.authority_fileprovider);
        Uri fileUri = FileProvider.getUriForFile(this, authority, imageToShare);

        Intent viewFile = new Intent(Intent.ACTION_SEND);
        String defaultShareText = getString(R.string.full_sized_gallery_image_share_default_text);

        viewFile.setDataAndType(fileUri, getContentResolver().getType(fileUri));
        viewFile.putExtra(Intent.EXTRA_STREAM, fileUri);
        viewFile.putExtra(Intent.EXTRA_TEXT, defaultShareText);
        viewFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        String shareChooserTitle = getString(R.string.full_sized_gallery_image_share_chooser_title);
        startActivity(Intent.createChooser(viewFile, shareChooserTitle));
    }

    private void openWith() {
        File imageToShare = new File(url);

        String authority = getString(R.string.authority_fileprovider);
        Uri fileUri = FileProvider.getUriForFile(this, authority, imageToShare);

        Intent openWithIntent = new Intent(Intent.ACTION_VIEW);
        String defaultOpenWithText = getString(R.string.full_sized_gallery_image_open_with_text);

        openWithIntent.setDataAndType(fileUri, getContentResolver().getType(fileUri));
        openWithIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        openWithIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivity(Intent.createChooser(openWithIntent, defaultOpenWithText));
    }

    private void printImage() {
        PrintHelper photoPrinter = new PrintHelper(FullSizedGalleryImageActivity.this);
        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
        Bitmap bitmap = BitmapFactory.decodeFile(url);
        photoPrinter.printBitmap(imageName + " - Print Job", bitmap);
    }

    private void getAdditionalDetails() {
        Intent intent = new Intent(
                FullSizedGalleryImageActivity.this, AdditionalImageDetailsActivity.class);
        intent.putExtra("Url", url);
        startActivity(intent);
    }

    private void deleteImageDialog() {
        LayoutInflater layoutInflater = LayoutInflater.from(FullSizedGalleryImageActivity.this);
        View deletionDialogueBox = layoutInflater.inflate(
                R.layout.deletion_dialog_box, null);
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(FullSizedGalleryImageActivity.this);
        alertDialogBuilder.setView(deletionDialogueBox);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(R.string.dialog_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        File fileToDelete = new File(url);
                        fileToDelete.delete();
                        Toast.makeText(
                                FullSizedGalleryImageActivity.this,
                                R.string.dialog_deleted,
                                Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                })
                .setNegativeButton(R.string.dialog_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
