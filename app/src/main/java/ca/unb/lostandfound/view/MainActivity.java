package ca.unb.lostandfound.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LifecycleOwner;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import ca.unb.lostandfound.R;

public class MainActivity extends AppCompatActivity implements LifecycleOwner {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fragment defaultFrag = new CameraPreviewFragment();

        if (savedInstanceState == null) {
            loadFragment(defaultFrag);
        }

        enableBottomNavigationBar();
    }

    public void enableBottomNavigationBar() {

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_main);
        bottomNavigationView.setSelectedItemId(R.id.bottom_nav_action_camera);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Intent intent;
                Fragment fragment;
                switch (item.getItemId()) {
                    case R.id.bottom_nav_action_camera:
                        fragment = new CameraPreviewFragment();
                        loadFragment(fragment);
                        break;
                    case R.id.bottom_nav_action_gallery:
                        fragment = new GalleryFragment();
                        loadFragment(fragment);
                        break;
                    case R.id.bottom_nav_action_qr_reader:
                        fragment = new QrReaderFragment();
                        loadFragment(fragment);
                        break;
                }
                return true;
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

}
