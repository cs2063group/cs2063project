package ca.unb.lostandfound.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import ca.unb.lostandfound.view.FullSizedGalleryImageActivity;
import ca.unb.lostandfound.view.util.CreateList;
import ca.unb.lostandfound.R;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {

    private ArrayList<CreateList> galleryList;
    private Context context;
    private final int IMAGE_WIDTH = 400;
    private final int IMAGE_HEIGHT = 600;

    public ImageAdapter(Context context, ArrayList<CreateList> galleryList) {
        this.galleryList = galleryList;
        this.context = context;
    }

    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.gallery_cell_layout, viewGroup, false
        );

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ImageAdapter.ViewHolder viewHolder, final int i) {
        File f = new File(galleryList.get(i).getImageUrl());
        Picasso.get()
                .load(f)
                .resize(IMAGE_WIDTH, IMAGE_HEIGHT)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(viewHolder.img);

        viewHolder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FullSizedGalleryImageActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("image_url", galleryList.get(i).getImageUrl());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return galleryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img;

        public ViewHolder(View view) {
            super(view);

            img = (ImageView) view.findViewById(R.id.gallery_img);
        }
    }
}
