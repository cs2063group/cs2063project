package ca.unb.lostandfound.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.LuminanceSource;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import ca.unb.lostandfound.R;

public class AdditionalImageDetailsActivity extends AppCompatActivity {

    private final String TAG = "AdditionalImageDetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_info);
        setTitle(R.string.additional_details_title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        String url = getIntent().getStringExtra("Url");
        File file = new File(url);

        // TitleView
        TextView titleView = findViewById(R.id.additional_details_title);
        titleView.setText(file.getName());
        // Size
        TextView sizeView = findViewById(R.id.image_size);
        float fileSizeMB = (float) file.length() / (float) (1024 * 1024);
        NumberFormat numberFormat = new DecimalFormat("#0.00");
        sizeView.setText(numberFormat.format(fileSizeMB) + " MB");

        String coordinatesMessage = null;
        String[] coordinates;

        try {
            coordinatesMessage = readQRCodeFromUrl(url);
        }
        catch (Exception e) {
            Log.d(TAG, "onCreate: An error occurred while reading the QR code.");
            e.printStackTrace();
        }

        if (coordinatesMessage != null) {
            coordinates = coordinatesMessage.split(",");
            final String LATITUDE = coordinates[0].trim();
            final String LONGITUDE = coordinates[1];
            final int MAPS_ZOOM_LEVEL = 19;
            // Latitude
            TextView latView = findViewById(R.id.image_latitude);
            latView.setText(LATITUDE);
            // Longitude
            TextView longView = findViewById(R.id.image_longitude);
            longView.setText(LONGITUDE);

            Button mapsButton = findViewById(R.id.additional_details_button);
            mapsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String locationUri = "geo:" +
                            LATITUDE + "," +
                            LONGITUDE + "?z=" +
                            MAPS_ZOOM_LEVEL;
                    Uri intentUri = Uri.parse(locationUri);
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, intentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            });
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private String readQRCodeFromUrl(String url) throws Exception {
        Bitmap bitmap = BitmapFactory.decodeFile(url);
        String coordinatesMessage;

        int[] intArray = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(intArray, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        LuminanceSource source = new RGBLuminanceSource(bitmap.getWidth(), bitmap.getHeight(), intArray);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(source));

        Reader reader = new QRCodeReader();
        Result result = reader.decode(binaryBitmap);
        coordinatesMessage = result.getText();

        return coordinatesMessage;
    }

}
