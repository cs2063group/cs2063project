package ca.unb.lostandfound.view;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;

import ca.unb.lostandfound.R;
import ca.unb.lostandfound.service.FileCoordinator;
import ca.unb.lostandfound.view.adapters.ImageAdapter;
import ca.unb.lostandfound.view.util.CreateList;

public class GalleryFragment extends Fragment {

    private static final String TAG = "GalleryFragment";

    private RecyclerView recyclerView;
    private RecyclerView.Adapter imageAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<CreateList> createLists;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        recyclerView = (RecyclerView) view.findViewById(R.id.gallery_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);

        layoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(layoutManager);

        createLists = prepareData();
        imageAdapter = new ImageAdapter(getContext(), createLists);
        recyclerView.setAdapter(imageAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (recyclerView != null) {
            createLists = prepareData();
            imageAdapter = new ImageAdapter(getContext(), createLists);
            recyclerView.setAdapter(imageAdapter);
        }
    }

    private ArrayList<CreateList> prepareData() {

        ArrayList<CreateList> image = new ArrayList<>();
        File[] files = FileCoordinator.getInstance().getMediaStorageFiles();
        if (files == null) {
            Log.d(TAG, "prepareData: No images have been found.");
        } else {
            for (File file : files) {
                Log.d(TAG, "File Info: " + file.toString() + " : " + file.getName());
                CreateList createList = new CreateList();
                createList.setImageUrl(file.toString());
                image.add(createList);
            }
        }
        return image;
    }

}
