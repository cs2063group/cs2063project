package ca.unb.lostandfound.view;

import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraX;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureConfig;
import androidx.camera.core.Preview;
import androidx.camera.core.PreviewConfig;
import androidx.fragment.app.Fragment;

import ca.unb.lostandfound.R;
import ca.unb.lostandfound.service.CameraUtils;
import ca.unb.lostandfound.service.OrientationEventListenerWrapper;
import ca.unb.lostandfound.service.PictureManager;
import ca.unb.lostandfound.service.UserLocationServices;

public class CameraPreviewFragment extends Fragment {

    private static final String TAG = "CameraPreviewFragment";

    private CameraUtils cameraUtils;
    private UserLocationServices locationServices;
    private TextureView textureView;
    private PictureManager pictureManager;

    private OrientationEventListenerWrapper orientationListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_camera_preview, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();

        textureView = view.findViewById(R.id.textureView);

        cameraUtils = new CameraUtils();
        locationServices = new UserLocationServices(getContext());
        pictureManager = new PictureManager(getContext());

        enableOrientationListener();

        getPermissions();

        final ImageCapture imageCapture = startCameraPreview();

        final ImageButton pictureButton = (ImageButton)view.findViewById(R.id.take_picture_button);
        pictureButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int orientation = getDeviceOrientation();

                pictureManager.getPermissions(getActivity());
                pictureManager.takeLostItemPicture(imageCapture, orientation);

                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                View descriptionDialogueBox = layoutInflater.inflate(
                        R.layout.description_dialog_box, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                alertDialogBuilder.setView(descriptionDialogueBox);

                final EditText descriptionEditText = (EditText) descriptionDialogueBox
                        .findViewById(R.id.description_dialog_box_user_input);
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton(R.string.dialog_submit, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String descriptionText = descriptionEditText.getText().toString();
                                int textSize = getResources()
                                        .getDimensionPixelSize(R.dimen.descriptionOverlay);
                                pictureManager.addDescriptionToLostItemPicture
                                        (descriptionText, textSize);
                                pictureManager.saveBitmap();

                            }
                        })
                        .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();


            }
        });

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

        // Need to disable or else it will drain the device battery
        // in the background
        orientationListener.disable();
    }

    @Override
    public void onResume() {
        super.onResume();
        orientationListener.enable();
    }

    public ImageCapture startCameraPreview() {

        // Unbind any existing cameras before setting a new one up
        CameraX.unbindAll();

        final PreviewConfig config = new PreviewConfig.Builder().build();
        final Preview preview = new Preview(config);

        preview.setOnPreviewOutputUpdateListener(
                new Preview.OnPreviewOutputUpdateListener() {
                    @Override
                    public void onUpdated(Preview.PreviewOutput previewOutput) {
                        textureView.setSurfaceTexture(previewOutput.getSurfaceTexture());
                    }
                });

        ImageCaptureConfig imageCaptureConfig =
                new ImageCaptureConfig
                        .Builder()
                        .build();

        final ImageCapture imageCapture = new ImageCapture(imageCaptureConfig);

        // Starts the camera
        CameraX.bindToLifecycle(this, preview, imageCapture);

        return imageCapture;
    }

    public int getDeviceOrientation() {
        int orientation = orientationListener.getLastOrientation();
        int genericOrientation;

        if (orientation == OrientationEventListenerWrapper.ORIENTATION_LANDSCAPE ||
                orientation == OrientationEventListenerWrapper.ORIENTATION_LANDSCAPE_REVERSE) {
            genericOrientation = Configuration.ORIENTATION_LANDSCAPE;
        }
        else {
            genericOrientation = Configuration.ORIENTATION_PORTRAIT;
        }

        return genericOrientation;
    }

    public void enableOrientationListener() {
        orientationListener = new OrientationEventListenerWrapper(getContext());
        orientationListener.enable();
    }

    public void getPermissions() {
        cameraUtils.getCameraPermissions(getActivity());
        locationServices.getLocationPermissions(getActivity());
    }
}
