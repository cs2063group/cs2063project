package ca.unb.lostandfound.service;

public final class PermissionCodes {
    public final static int CAMERA = 100;
    public final static int COARSE_LOCATION = 101;
    public final static int FINE_LOCATION = 102;
    public final static int WRITE_EXTERNAL_STORAGE = 103;
    public final static int READ_EXTERNAL_STORAGE = 104;
}
