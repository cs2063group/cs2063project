package ca.unb.lostandfound.service;

import android.graphics.Bitmap;

public interface PictureTakenCallback {

    void onSuccess(Bitmap bitmap);
}
