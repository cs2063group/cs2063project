package ca.unb.lostandfound.service;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.Image;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageProxy;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.File;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CameraUtils {

    private final String TAG = "CameraUtils";

    private final int REQUEST_CODE_PERMISSIONS[] = {PermissionCodes.CAMERA, PermissionCodes.WRITE_EXTERNAL_STORAGE};
    private final String[] REQUIRED_PERMISSIONS
            = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};

    public void getCameraPermissions(Activity activity) {
        for (int i = 0; i < REQUIRED_PERMISSIONS.length; i++) {
            String permission = REQUIRED_PERMISSIONS[i];
            int permissionCode = REQUEST_CODE_PERMISSIONS[i];
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(activity, REQUIRED_PERMISSIONS, permissionCode);
            }
        }
    }

    public void takePictureAsBitmap(final ImageCapture imageCapture, final int orientation, final PictureTakenCallback pictureTakenCallback) {
        imageCapture.takePicture(new ImageCapture.OnImageCapturedListener() {

            @Override
            public void onCaptureSuccess(ImageProxy image, int rotationDegrees) {
                // Convert the image into a bitmap
                Image imageTaken = image.getImage();
                ByteBuffer buffer = imageTaken.getPlanes()[0].getBuffer();
                byte[] bytes = new byte[buffer.capacity()];
                buffer.get(bytes);
                Bitmap imageBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null);


                // Assume that the camera defaults to landscape mode
                // If the device is in potrait orientation, the image
                // will need to be rotated. If not, no rotation.
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    int rotation = 90;
                    imageBitmap = rotateBitmap(imageBitmap,  rotation);
                }

                Log.d(TAG, "Successfully created bitmap from captured image.");
                pictureTakenCallback.onSuccess(imageBitmap);

                super.onCaptureSuccess(image, rotationDegrees);
            }

            @Override
            public void onError(ImageCapture.UseCaseError useCaseError, String message, @Nullable Throwable cause) {
                super.onError(useCaseError, message, cause);
                Log.d(TAG, "Error occurred when taking a picture.");
            }
        });
    }

    private Bitmap rotateBitmap(Bitmap targetBitmap, int degrees) {
        int width = targetBitmap.getWidth();
        int height = targetBitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Bitmap rotatedBitmap = Bitmap.createBitmap(targetBitmap, 0, 0, width, height, matrix, true);

        return rotatedBitmap;
    }

    private File getImageFile(){

        File mediaStorageDir = FileCoordinator.getInstance().getMediaStorageDir();

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");

        return mediaFile;
    }
}
