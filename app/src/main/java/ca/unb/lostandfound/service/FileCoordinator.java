package ca.unb.lostandfound.service;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import ca.unb.lostandfound.LostAndFoundApplication;

public class FileCoordinator {

    private final String TAG = "FileCoordinator";

    private static FileCoordinator instance;
    private File mediaStorageDir;

    private FileCoordinator() {
        LostAndFoundApplication app = LostAndFoundApplication.getInstance();
        mediaStorageDir = app.getApplicationMediaStorageDir();
        createMediaDir();
    }

    private void createMediaDir() {
        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d(TAG, "Failed to create image directory.");
            }
        }
    }

    public static FileCoordinator getInstance() {
        if (instance == null) {
            instance = new FileCoordinator();
        }

        return instance;
    }

    public File getMediaStorageDir() {
        return mediaStorageDir;
    }

    public File[] getMediaStorageFiles() {
        File[] files = mediaStorageDir.listFiles();
        return files;
    }

    public void saveBitmapToFile(Bitmap bitmapToSave) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String filename =  "IMG_"+ timeStamp + ".png";

        File destinationFile = new File(mediaStorageDir, filename);

        try {
            FileOutputStream out = new FileOutputStream(destinationFile);
            bitmapToSave.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
