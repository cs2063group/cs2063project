package ca.unb.lostandfound.service;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class UserLocationServices {

    private final String TAG = "UserLocationServices";
    private final int[] REQUEST_CODE_PERMISSIONS = {PermissionCodes.FINE_LOCATION};
    private final String[] REQUIRED_PERMISSIONS
            = new String[]{"android.permission.ACCESS_FINE_LOCATION"};
    private LocationManager locationManager;


    public UserLocationServices(Context context) {
        this.locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
    }

    public LatLng getCurrentLocation() {
        Location location = determineBestLocation();

        try {
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            LatLng userLocation = new LatLng(latitude, longitude);
            Log.d(TAG, "Retrieved user location: Latitude: " + String.valueOf(latitude) + " Longitude: " + String.valueOf(longitude));
            return userLocation;
        }
        catch (NullPointerException npEx) {
            npEx.printStackTrace();
            Log.d(TAG, "Failed to retrieve the user's current location.");
            return null;
        }
    }

    public void getLocationPermissions(Activity activity) {
        for (int i = 0; i < REQUIRED_PERMISSIONS.length; i++) {
            String permission = REQUIRED_PERMISSIONS[i];
            int permissionCode = REQUEST_CODE_PERMISSIONS[i];
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(activity, REQUIRED_PERMISSIONS, permissionCode);
            }
        }
    }

    private Location determineBestLocation() {
        Location bestLocation = null;
        try {
            // Use multiple providers to increase the chance of getting a location
            List<String> providers = locationManager.getProviders(true);
            for (String provider : providers) {
                Location currentLocation = locationManager.getLastKnownLocation(provider);

                if (currentLocation != null) {
                    if (bestLocation == null || currentLocation.getAccuracy() < bestLocation.getAccuracy()) {
                        bestLocation = currentLocation;
                    }
                }
            }

        }
        catch (SecurityException securityEx) {
            Log.d(TAG, "Unable to retrieve user location due to lack of permissions.");
            return null;
        }

        return bestLocation;
    }
}
