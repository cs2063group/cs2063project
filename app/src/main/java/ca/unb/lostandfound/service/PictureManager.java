package ca.unb.lostandfound.service;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.media.MediaActionSound;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.util.Log;

import androidx.camera.core.ImageCapture;

import com.google.android.gms.maps.model.LatLng;
import com.google.zxing.WriterException;

public class PictureManager {

    private final String TAG = "PictureManager";
    private final double QR_CODE_SIZE_RATIO = 1.0/5.0;
    private final double MAX_TEXT_WIDTH_RATIO = 3.0/4.0;
    private final float VERTICAL_TEXT_PADDING = 400;

    private UserLocationServices locationServices;
    private CameraUtils cameraUtils;
    private QrCodeEncoder qrCodeEncoder;
    private Bitmap resultInMemory;
    private int pictureHeight;

    public PictureManager(Context context) {
        this.locationServices = new UserLocationServices(context);
        this.cameraUtils = new CameraUtils();
        this.qrCodeEncoder = new QrCodeEncoder();
    }

    public void getPermissions(Activity activity) {
        locationServices.getLocationPermissions(activity);
        cameraUtils.getCameraPermissions(activity);
    }

    public void takeLostItemPicture(final ImageCapture imageCapture, int orientation) {
        playCameraSound();
        cameraUtils.takePictureAsBitmap(imageCapture, orientation,new PictureTakenCallback() {
            @Override
            public void onSuccess(Bitmap pictureTakenBitmap) {
                int qrCodeSize = (int)Math.floor(pictureTakenBitmap.getHeight() * QR_CODE_SIZE_RATIO);
                Bitmap locationBitmap = getLocationBitmap(qrCodeSize);
                pictureHeight = pictureTakenBitmap.getHeight();

                if (locationBitmap != null) {
                    resultInMemory = addQrCodeToPictureTaken(pictureTakenBitmap, locationBitmap);
                }
            }
        });
    }

    public void addDescriptionToLostItemPicture(String description, int textSize) {
        Canvas canvas = new Canvas(resultInMemory);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
        paint = calculateTextSize(textSize, paint, description);

        float verticalTextPadding = canvas.getHeight() - 350f;
        float descriptionXPosition = 10;
        float descriptionYPosition = verticalTextPadding;
        canvas.drawText(description, descriptionXPosition, descriptionYPosition, paint);
    }


    public void saveBitmap() {
        if (resultInMemory != null) {
            new BitmapSaverTask().execute(resultInMemory);
        }

    }

    private Bitmap addQrCodeToPictureTaken(Bitmap pictureBitmap, Bitmap qrCodeBitmap) {
        // Add additional space to the height of the image
        int height = pictureBitmap.getHeight() + (int)Math.floor(pictureBitmap.getHeight() * QR_CODE_SIZE_RATIO);
        Bitmap combinedResultBitmap = Bitmap
                .createBitmap(pictureBitmap.getWidth(), height, pictureBitmap.getConfig());
        Canvas canvas = new Canvas(combinedResultBitmap);

        // Sets the background colour of unused space
        canvas.drawColor(Color.WHITE);

        float pictureLeftPosition = 0f;
        float pictureTopPosition = 0f;
        canvas.drawBitmap(pictureBitmap, pictureLeftPosition, pictureTopPosition, null);

        // These positions will put the QR Code in the bottom right corner
        float qrCodeLeftPosition = pictureBitmap.getWidth() - qrCodeBitmap.getWidth();
        float qrCodeTopPosition = pictureBitmap.getHeight();
        canvas.drawBitmap(qrCodeBitmap, qrCodeLeftPosition, qrCodeTopPosition, null);

        return combinedResultBitmap;
    }

    private Bitmap getLocationBitmap(int bitmapSize) {
        Bitmap locationBitmap = null;

        try {
            LatLng location = locationServices.getCurrentLocation();
            String locationData = location.latitude + ", " + location.longitude;
            locationBitmap = qrCodeEncoder.encodeAsBitmap(locationData, bitmapSize);
        }
        catch (NullPointerException npEx) {
            Log.d(TAG, "No location data provided to encode as bitmap.");
        }
        catch (WriterException writerEx) {
            Log.d(TAG, "Unable to encode the location as a bitmap.");
        }

        return locationBitmap;
    }

    private void playCameraSound() {
        MediaActionSound sound = new MediaActionSound();
        sound.play(MediaActionSound.SHUTTER_CLICK);
    }

    private Paint calculateTextSize(int textSize, Paint paint, String description) {
        Rect bounds = new Rect();
        paint.setTextSize(textSize);
        paint.getTextBounds(description, 0, description.length(), bounds);
        while (bounds.width() > resultInMemory.getWidth() * MAX_TEXT_WIDTH_RATIO) {
            textSize--;
            paint.setTextSize(textSize);
            paint.getTextBounds(description, 0, description.length(), bounds);
        }
        return paint;
    }
}
