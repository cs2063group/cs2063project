package ca.unb.lostandfound.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import ca.unb.lostandfound.LostAndFoundApplication;
import ca.unb.lostandfound.R;

public class BitmapSaverTask extends AsyncTask<Bitmap, Void, Boolean> {

    private final String TAG = "BitmapSaverTask";

    @Override
    protected Boolean doInBackground(Bitmap... bitmaps) {

        if (bitmaps[0] == null) {
            return false;
        }

        FileCoordinator fileCoordinator = FileCoordinator.getInstance();
        fileCoordinator.saveBitmapToFile(bitmaps[0]);

        return true;
    }

    @Override
    protected void onPostExecute(Boolean saveSuccessful) {
        super.onPostExecute(saveSuccessful);
        Log.d(TAG, "Finished executing");

        String toastMsg;
        Context context = LostAndFoundApplication
                .getInstance()
                .getApplicationContext();

        if (saveSuccessful) {
            toastMsg = context
                    .getResources()
                    .getString(R.string.toast_picture_taken_success);
            Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT).show();
        }
        else {
            toastMsg = LostAndFoundApplication
                    .getInstance()
                    .getResources()
                    .getString(R.string.toast_picture_taken_failure);
            Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT).show();
        }
    }

}
