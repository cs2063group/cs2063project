package ca.unb.lostandfound.service;

import android.util.Log;

public class CoordinateUtils {

    private static final String TAG =  "CoordinateUtils";

    public static double[] parseCoordinates(String coordinatesStr) {
        double latitude;
        double longitude;

        try {
            String[] coordinatesSplit = coordinatesStr.split(",");
            latitude = Double.parseDouble(coordinatesSplit[0]);
            longitude = Double.parseDouble(coordinatesSplit[1]);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            Log.d(TAG, "Error when parsing coordinate values.");
            return new double[0];
        }

        double[] coordinates = {latitude, longitude};
        return coordinates;
    }

    public static boolean validateCoordinates(double[] coordinates) {
        double latitude = coordinates[0];
        double longitude = coordinates[1];

        return checkValidLatitude(latitude) && checkValidLongitude(longitude);
    }

    public static boolean checkValidLatitude(double latitude) {
        if (latitude < -90 || latitude > 90) {
            Log.d(TAG, "Latitude value is not valid. [Latitude = " + latitude + "]");
            return false;
        }

        return true;
    }

    public static boolean checkValidLongitude(double longitude) {
        if (longitude < -180 || longitude > 180) {
            Log.d(TAG, "Longitude value is not valid. [Longitude = " + longitude + "]");
            return false;
        }

        return true;
    }
}
