package ca.unb.lostandfound.service;

import android.content.Context;
import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class QrCodeEncoder {

    private final int WHITE = 0xFFFFFFFF;
    private final int BLACK = 0xFF000000;

    public Bitmap encodeAsBitmap(String stringToEncode, int bitmapWidth) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(stringToEncode,
                    BarcodeFormat.QR_CODE, bitmapWidth, bitmapWidth, null);
        } catch (IllegalArgumentException illegalArgEx) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, bitmapWidth, 0, 0, w, h);
        return bitmap;
    }
}
