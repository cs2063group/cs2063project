package ca.unb.lostandfound;

import android.app.Application;

import java.io.File;

public class LostAndFoundApplication extends Application {

    private static LostAndFoundApplication instance;

    public static LostAndFoundApplication getInstance(){
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public File getApplicationMediaStorageDir() {
        String dirName = getResources().getString(R.string.app_name);
        File mediaStorageDir = new File(getExternalFilesDir(null), dirName);

        return mediaStorageDir;
    }
}
